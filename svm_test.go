package svm

import (
	"testing"
)

func TestPowi(t *testing.T) {
	var v float64
	v = powi(2.0, 16)
	if v != 65536 {
		t.Error("Expected 65536, got ", v)
	}
}

// func TestSize(t *testing.T) {
// 	var ht *headT = new(headT)
// 	sizeOfHeadT := unsafe.SizeOf(ht)
// 	if sizeOfHeadT != 32 {
// 		t.Error("Expected 65536, got ", sizeOfHeadT)
// 	}
// }
