package svm

import (
	"bufio"
	"fmt"
	"io"
	"log"
	"math"
	"math/rand"
	"os"
	"strconv"
	"strings"
)

const (
	LIBSVM_VERSION = 322
	TAU            = 1e-12
	INF            = math.MaxFloat64
)

func powi(base float64, times int) float64 {
	tmp := base
	ret := 1.0

	for t := times; t > 0; t /= 2 {
		if t%2 == 1 {
			ret *= tmp
		}
		tmp *= tmp
	}
	return ret
}

// TODO use this to get size
// func getSize(T interface{}) uintptr {
//      v := reflect.TypeOf(T).Size()
//      return v
// }

type Cacher interface {
	lruDelete(h *headT)
	lruInsert(h *headT)
	// request data [0,len)
	// return some position p where [p,len) need to be filled
	// (p >= len if nothing needs to be filled)
	getData(index int, data [][]float64, length int)
}

type headT struct {
	// a circular list
	prev, next *headT
	data       []float32
	// data[0,len) is cached in this entry
	length int
}

//
// Kernel Cache
//
// l is the number of total data items
// size is the cache size limit in bytes
//
type Cache struct {
	l       int
	size    int64
	head    []*headT
	lruHead *headT
}

func NewCache(l int, size int64) *Cache {
	c := Cache{
		l:       l,
		size:    size,
		head:    make([]*headT, l), // initialized to 0
		lruHead: new(headT)}
	c.size /= 4 // sizeof(Qfloat) float32 sizeof (float32(0))
	// Change this magic numbers --> getSize
	c.size -= int64(l * (16 / 4)) // sizeof(head_t) / sizeof(Qfloat) sizeof(headT{})
	// cache must be large enough for two columns
	c.size = int64(math.Max(float64(c.size), float64(2*c.l)))
	c.lruHead.prev = c.lruHead
	c.lruHead.next = c.lruHead
	return &c
}

func (c *Cache) lruDelete(h *headT) {
	h.prev.next = h.next
	h.next.prev = h.prev
}

func (c *Cache) lruInsert(h *headT) {
	// insert to last position
	h.next = c.lruHead
	h.prev = c.lruHead.prev
	h.prev.next = h
	h.next.prev = h
}

func (c *Cache) getData(index int, data [][]float32, length int) int {
	h := c.head[index]
	if h.length > 0 {
		c.lruDelete(h)
	}
	more := length - h.length
	if more > 0 {
		// free old space
		for c.size < int64(more) {
			old := c.lruHead.next
			c.lruDelete(old)
			c.size += int64(old.length)
			old.data = nil
			old.length = 0
		}
		// allocate new space
		newData := make([]float32, length)
		if h.data != nil {
			copy(newData, h.data[:h.length])
		}
		h.data = newData
		c.size -= int64(more)
		h.length, length = length, h.length
	}
	c.lruInsert(h)
	data[0] = h.data
	return length
}

func (c *Cache) swapIndex(i, j int) {
	if i == j {
		return
	}
	if c.head[i].length > 0 {
		c.lruDelete(c.head[i])
	}
	if c.head[j].length > 0 {
		c.lruDelete(c.head[j])
	}
	c.head[i].data, c.head[j].data = c.head[j].data, c.head[i].data
	c.head[i].length, c.head[j].length = c.head[j].length, c.head[i].length
	if c.head[i].length > 0 {
		c.lruInsert(c.head[i])
	}
	if c.head[j].length > 0 {
		c.lruInsert(c.head[j])
	}
	if i > j {
		i, j = j, i
	}
	for h := c.lruHead.next; h != c.lruHead; h = h.next {
		if h.length <= i {
			continue
		}
		if h.length > j {
			h.data[i], h.data[j] = h.data[j], h.data[i]
		} else {
			// give up
			c.lruDelete(h)
			c.size += int64(h.length)
			h.data = nil
			h.length = 0
		}
	}
}

type svmNode struct {
	index int
	value float64
}

type svmParameter struct {
	svmType    int
	kernelType int
	degree     int     // for poly
	gamma      float64 // for poly/rbf/sigmoid
	coef0      float64 // for poly/sigmoid

	// these are for training only
	cacheSize   float64   // in MB
	eps         float64   // stopping criteria
	C           float64   // for C_SVC, EPSILON_SVR and NU_SVR
	nrWeight    int       // for C_SVC
	weightLabel []int     // for C_SVC
	weight      []float64 // for C_SVC
	nu          float64   // for NU_SVC, ONE_CLASS, and NU_SVR
	p           float64   // for EPSILON_SVR
	shrinking   int       // use the shrinking heuristics
	probability int       // do probability estimates
}

type svmProblem struct {
	l int
	y []float64
	x [][]svmNode
}

type svmModel struct {
	param     *svmParameter // parameter
	nrClass   int           // number of classes, = 2 in regression/one class svm
	l         int           // total #SV
	SV        [][]svmNode   // SVs (SV[l])
	svCoef    [][]float64   // coefficients for SVs in decision functions (sv_coef[k-1][l])
	rho       []float64     // constants in decision functions (rho[k*(k-1)/2])
	probA     []float64     // pariwise probability information
	probB     []float64
	svIndices []int // sv_indices[0,...,nSV-1] are values in [1,...,num_traning_data] to indicate SVs in the training set

	// for classification only
	label []int // label of each class (label[k])
	nSV   []int // number of SVs for each class (nSV[k])
	// nSV[0] + nSV[1] + ... + nSV[k-1] = l
}

// svm_type
const (
	C_SVC = iota
	NU_SVC
	ONE_CLASS
	EPSILON_SVR
	NU_SVR
)

// kernel_type
const (
	LINEAR = iota
	POLY
	RBF
	SIGMOID
	PRECOMPUTED
)

//
// Kernel evaluation
//
// the static method k_function is for doing single kernel evaluation
// the constructor of Kernel prepares to calculate the l*l kernel matrix
// the member function get_Q is for getting one column from the Q Matrix
//
type QMatrix interface {
	getQ(column, len int) []float32
	getQD() []float64
	swapIndex(i, j int)
}

type Kernel struct {
	x       [][]svmNode
	xSquare []float64

	// svm_parameter
	kernelType int
	degree     int
	gamma      float64
	coef0      float64
}

func (k *Kernel) swapIndex(i, j int) {
	k.x[i], k.x[j] = k.x[j], k.x[i]
	if k.xSquare != nil {
		k.xSquare[i], k.xSquare[j] = k.xSquare[j], k.xSquare[i]
	}
}

func (k *Kernel) kernelFunction(i, j int) float64 {
	switch k.kernelType {
	case LINEAR:
		return dot(k.x[i], k.x[j])
	case POLY:
		return powi(k.gamma*dot(k.x[i], k.x[j])+k.coef0, k.degree)
	case RBF:
		return math.Exp(-k.gamma * (k.xSquare[i] + k.xSquare[j] - 2*dot(k.x[i], k.x[j])))
	case SIGMOID:
		return math.Tanh(k.gamma*dot(k.x[i], k.x[j]) + k.coef0)
	case PRECOMPUTED:
		return k.x[i][int(k.x[j][0].value)].value
	}
	return 0.0
}

func dot(px, py []svmNode) float64 {
	var sum float64 = 0.0
	i, j, lengthX, lengthY := 0, 0, len(px), len(py)
	for i < lengthX && j < lengthY {
		if px[i].index == py[j].index {
			sum += px[i].value * py[j].value
			i++
			j++
		} else if px[i].index > py[j].index {
			j++
		} else {
			i++
		}
	}
	return sum
}

func (k *Kernel) kernelLinear(i, j int) float64 {
	return dot(k.x[i], k.x[j])
}

func NewKernel(l int, x [][]svmNode, param *svmParameter) *Kernel {
	k := Kernel{
		x:          make([][]svmNode, l),
		kernelType: param.kernelType,
		degree:     param.degree,
		gamma:      param.gamma,
		coef0:      param.coef0}

	copy(k.x, x)

	if k.kernelType == RBF {
		k.xSquare = make([]float64, l)
		for index, value := range k.x {
			k.xSquare[index] = dot(value, value)
		}
	}
	return &k
}

func kFunction(x, y []svmNode, param *svmParameter) float64 {
	switch param.kernelType {
	case LINEAR:
		return dot(x, y)
	case POLY:
		return powi(param.gamma*dot(x, y)+param.coef0, param.degree)
	case RBF:
		var sum float64 = 0.0
		i, j, lengthX, lengthY := 0, 0, len(x), len(y)
		for i < lengthX && j < lengthY {
			if x[i].index == y[j].index {
				d := x[i].value - y[j].value
				sum += d * d
				i++
				j++
			} else if x[i].index > y[j].index {
				sum += y[j].value * y[j].value
				j++
			} else {
				sum += x[i].value * x[i].value
				i++
			}
		}
		for i < lengthX {
			sum += x[i].value * x[i].value
			i++
		}
		for j < lengthY {
			sum += y[j].value * y[j].value
			j++
		}
		return math.Exp(-param.gamma * sum)
	case SIGMOID:
		return math.Tanh(param.gamma*dot(x, y) + param.coef0)
	case PRECOMPUTED:
		return x[int(y[0].value)].value
	}
	return 0.0
}

// An SMO algorithm in Fan et al., JMLR 6(2005), p. 1889--1918
// Solves:
//
//	min 0.5(\alpha^T Q \alpha) + p^T \alpha
//
//		y^T \alpha = \delta
//		y_i = +1 or -1
//		0 <= alpha_i <= Cp for y_i = 1
//		0 <= alpha_i <= Cn for y_i = -1
//
// Given:
//
//	Q, p, y, Cp, Cn, and an initial feasible point \alpha
//	l is the size of vectors and matrices
//	eps is the stopping tolerance
//
// solution will be put in \alpha, objective value will be put in obj
//
const (
	LOWER_BOUND byte = iota
	UPPER_BOUND
	FREE
)

type SolutionInfo struct {
	obj         float64
	rho         float64
	upperBoundP float64
	upperBoundN float64
	r           float64 // for Solver_NU
}

type Solver struct {
	activeSize  int
	y           []int8
	G           []float64 // gradient of objective function
	alphaStatus []byte    // LOWER_BOUND, UPPER_BOUND, FREE
	alpha       []float64
	Q           QMatrix
	QD          []float64
	eps         float64
	Cp, Cn      float64
	p           []float64
	activeSet   []int
	GBar        []float64 // gradient, if we treat free variables as 0
	l           int
	unshrink    bool // XXX
}

func (s *Solver) getC(i int) float64 {
	if s.y[i] > 0 {
		return s.Cp
	} else {
		return s.Cn
	}
}

func (s *Solver) updateAlphaStatus(i int) {
	if s.alpha[i] >= s.getC(i) {
		s.alphaStatus[i] = UPPER_BOUND
	} else if s.alpha[i] <= 0 {
		s.alphaStatus[i] = LOWER_BOUND
	} else {
		s.alphaStatus[i] = FREE
	}
}

func (s *Solver) isUpperBound(i int) bool {
	return s.alphaStatus[i] == UPPER_BOUND
}

func (s *Solver) isLowerBound(i int) bool {
	return s.alphaStatus[i] == LOWER_BOUND
}

func (s *Solver) isFree(i int) bool {
	return s.alphaStatus[i] == FREE
}

func (s *Solver) swapIndex(i, j int) {
	s.Q.swapIndex(i, j)
	s.y[i], s.y[j] = s.y[j], s.y[i]
	s.G[i], s.G[j] = s.G[j], s.G[i]
	s.alphaStatus[i], s.alphaStatus[j] = s.alphaStatus[j], s.alphaStatus[i]
	s.alpha[i], s.alpha[j] = s.alpha[j], s.alpha[i]
	s.p[i], s.p[j] = s.p[j], s.p[i]
	s.activeSet[i], s.activeSet[j] = s.activeSet[j], s.activeSet[i]
	s.GBar[i], s.GBar[j] = s.GBar[j], s.GBar[i]
}

func (s *Solver) reconstructGradient() {
	// reconstruct inactive elements of G from G_bar and free variables
	if s.activeSize == s.l {
		return
	}
	var i, j int
	nrFree := 0
	for j = s.activeSize; j < s.l; j++ {
		s.G[j] = s.GBar[j] + s.p[j]
	}
	for j = 0; j < s.activeSize; j++ {
		if s.isFree(j) {
			nrFree++
		}
	}
	if 2*nrFree < s.activeSize {
		log.Println("WARNING: using -h 0 may be faster")
	}
	if nrFree*s.l > 2*s.activeSize*(s.l-s.activeSize) {
		for i = s.activeSize; i < s.l; i++ {
			Qi := s.Q.getQ(i, s.activeSize)
			for j = 0; j < s.activeSize; j++ {
				if s.isFree(j) {
					s.G[i] += s.alpha[j] * float64(Qi[j])
				}
			}
		}
	} else {
		for i = 0; i < s.activeSize; i++ {
			if s.isFree(i) {
				Qi := s.Q.getQ(i, s.l)
				alphaI := s.alpha[i]
				for j = s.activeSize; j < s.l; j++ {
					s.G[j] += alphaI * float64(Qi[j])
				}
			}
		}
	}
}

func (s *Solver) Solve(l int, Q QMatrix, p []float64, y []int8, alpha []float64,
	Cp, Cn, eps float64, si *SolutionInfo, shrinking int) {

	s.l = l
	s.Q = Q
	s.QD = Q.getQD()
	s.p = make([]float64, l)
	copy(s.p, p)
	s.y = make([]int8, l)
	copy(s.y, y)
	s.alpha = make([]float64, l)
	copy(s.alpha, alpha)
	s.Cp, s.Cn, s.eps = Cp, Cn, eps
	s.unshrink = false

	// initialize alpha_status
	{
		s.alphaStatus = make([]byte, l)
		for i := 0; i < l; i++ {
			s.updateAlphaStatus(i)
		}
	}

	// initialize active set (for shrinking)
	{
		s.activeSet = make([]int, l)
		for i := 0; i < l; i++ {
			s.activeSet[i] = i
		}
		s.activeSize = l
	}

	// initialize gradient
	s.G = make([]float64, l)
	s.GBar = make([]float64, l)
	var i int
	for i = 0; i < l; i++ {
		s.G[i] = s.p[i]
		s.GBar[i] = 0
	}
	for i = 0; i < l; i++ {
		if !s.isLowerBound(i) {
			Qi := s.Q.getQ(i, l)
			alphaI := s.alpha[i]
			var j int
			for j = 0; j < l; j++ {
				s.G[j] += alphaI * float64(Qi[j])
			}
			if s.isUpperBound(i) {
				for j = 0; j < l; j++ {
					s.GBar[j] += s.getC(i) * float64(Qi[j])
				}
			}
		}
	}

	// optimization step
	iter := 0
	max := float64(100 * l)
	if l > int(math.MaxInt32/100) {
		max = float64(math.MaxInt32)
	}
	maxIter := int(math.Max(float64(10000000), max))
	counter := int(math.Min(float64(l), float64(1000)) + 1)
	workingSet := make([]int, 2)

	for iter < maxIter {
		// show progress and do shrinking
		if counter--; counter == 0 {
			counter = int(math.Min(float64(l), float64(1000)))
			if shrinking != 0 {
				s.doShrinking()
			}
			log.Print(".")
		}
		if s.selectWorkingSet(workingSet) != 0 {
			// reconstruct the whole gradient
			s.reconstructGradient()
			// reset active set size and check
			s.activeSize = l
			log.Print("*")
			if s.selectWorkingSet(workingSet) != 0 {
				break
			} else {
				counter = 1 // do shrinking next iteration
			}
		}
		i, j := workingSet[0], workingSet[1]

		iter++

		// update alpha[i] and alpha[j], handle bounds carefully
		Qi := s.Q.getQ(i, s.activeSize)
		Qj := s.Q.getQ(j, s.activeSize)

		Ci := s.getC(i)
		Cj := s.getC(j)

		oldAlphaI := s.alpha[i]
		oldAlphaJ := s.alpha[j]

		if s.y[i] != s.y[j] {
			quadCoef := s.QD[i] + s.QD[j] + float64(2.0*Qi[j])
			if quadCoef <= 0.0 {
				quadCoef = TAU
			}
			delta := (-s.G[i] - s.G[j]) / quadCoef
			diff := s.alpha[i] - s.alpha[j]
			alpha[i] += delta
			alpha[j] += delta

			if diff > 0 {
				if alpha[j] < 0 {
					alpha[j] = 0
					alpha[i] = diff
				}
			} else {
				if alpha[i] < 0 {
					alpha[i] = 0
					alpha[j] = -diff
				}
			}
			if diff > Ci-Cj {
				if alpha[i] > Ci {
					alpha[i] = Ci
					alpha[j] = Ci - diff
				}
			} else {
				if alpha[j] > Cj {
					alpha[j] = Cj
					alpha[i] = Cj + diff
				}
			}
		} else {
			quadCoef := s.QD[i] + s.QD[j] - float64(2.0*Qi[j])
			if quadCoef <= 0.0 {
				quadCoef = TAU
			}
			delta := (s.G[i] - s.G[j]) / quadCoef
			sum := s.alpha[i] + s.alpha[j]
			alpha[i] -= delta
			alpha[j] += delta
			if sum > Ci {
				if alpha[i] > Ci {
					alpha[i] = Ci
					alpha[j] = sum - Ci
				}
			} else {
				if alpha[j] < 0 {
					alpha[j] = 0
					alpha[i] = sum
				}
			}
			if sum > Cj {
				if alpha[j] > Cj {
					alpha[j] = Cj
					alpha[i] = sum - Cj
				}
			} else {
				if alpha[i] < 0 {
					alpha[i] = 0
					alpha[j] = sum
				}
			}
		}
		// update G
		deltaAlphaI := alpha[i] - oldAlphaI
		deltaAlphaJ := alpha[j] - oldAlphaJ

		for k := 0; k < s.activeSize; k++ {
			s.G[k] += float64(Qi[k])*deltaAlphaI + float64(Qj[k])*deltaAlphaJ
		}
		// update alpha_status and G_bar
		{
			ui := s.isUpperBound(i)
			uj := s.isUpperBound(j)
			s.updateAlphaStatus(i)
			s.updateAlphaStatus(j)
			var k int
			// TODO DRY
			if ui != s.isUpperBound(i) {
				Qi = s.Q.getQ(i, l)
				if ui {
					for k = 0; k < l; k++ {
						s.GBar[k] -= Ci * float64(Qi[k])
					}
				} else {
					for k = 0; k < l; k++ {
						s.GBar[k] += Ci * float64(Qi[k])
					}
				}
			}
			if uj != s.isUpperBound(j) {
				Qj = s.Q.getQ(j, l)
				if uj {
					for k = 0; k < l; k++ {
						s.GBar[k] -= Cj * float64(Qj[k])
					}
				} else {
					for k = 0; k < l; k++ {
						s.GBar[k] += Cj * float64(Qj[k])
					}
				}
			}
		}
	}
	if iter >= maxIter {
		if s.activeSize < l {
			// reconstruct the whole gradient to calculate objective value
			s.reconstructGradient()
			s.activeSize = l
			log.Print("*")
		}
	}
	// calculate rho
	si.rho = s.calculateRho()

	// calculate objective value
	{
		var v float64 = 0.0
		var i int
		for i = 0; i < l; i++ {
			v += s.alpha[i] * (s.G[i] + s.p[i])
		}
		si.obj = v / 2.0
	}

	// put back the solution
	{
		for i := 0; i < l; i++ {
			alpha[s.activeSet[i]] = s.alpha[i]
		}
	}
	si.upperBoundP = Cp
	si.upperBoundN = Cn

	log.Printf("\nOptimization finished, #iter = %d\n", iter)
}

// return 1 if already optimal, return 0 otherwise
func (s *Solver) selectWorkingSet(workingSet []int) int {
	// return i,j such that
	// i: maximizes -y_i * grad(f)_i, i in I_up(\alpha)
	// j: mimimizes the decrease of obj value
	//    (if quadratic coefficeint <= 0, replace it with tau)
	//    -y_j*grad(f)_j < -y_i*grad(f)_i, j in I_low(\alpha)

	Gmax := -INF
	Gmax2 := -INF
	GmaxIdx := -1
	GminIdx := -1
	objDiffMin := INF

	for t := 0; t < s.activeSize; t++ {
		if s.y[t] == +1 {
			if !s.isUpperBound(t) {
				if -s.G[t] >= Gmax {
					Gmax = -s.G[t]
					GmaxIdx = t
				}
			}
		} else {
			if !s.isLowerBound(t) {
				if s.G[t] >= Gmax {
					Gmax = s.G[t]
					GmaxIdx = t
				}
			}
		}
	}
	i := GmaxIdx
	var Qi []float32
	if i != -1 { // NULL Qi not accessed: Gmax=-INF if i=-1
		Qi = s.Q.getQ(i, s.activeSize)
	}
	for j := 0; j < s.activeSize; j++ {
		// TODO DRY
		if s.y[j] == +1 {
			if !s.isLowerBound(j) {
				gradDiff := Gmax + s.G[j]
				if s.G[j] >= Gmax2 {
					Gmax2 = s.G[j]
				}
				if gradDiff > 0.0 {
					var objDiff float64
					quadCoef := s.QD[i] + s.QD[j] - float64(2.0*float32(s.y[i])*Qi[j])
					if quadCoef > 0.0 {
						objDiff = -(gradDiff * gradDiff) / quadCoef
					} else {
						objDiff = -(gradDiff * gradDiff) / TAU
					}
					if objDiff <= objDiffMin {
						GminIdx = j
						objDiffMin = objDiff
					}
				}
			}
		} else {
			if !s.isUpperBound(j) {
				gradDiff := Gmax - s.G[j]
				if -s.G[j] >= Gmax2 {
					Gmax2 = s.G[j]
				}
				if gradDiff > 0.0 {
					var objDiff float64
					quadCoef := s.QD[i] + s.QD[j] + float64(2.0*float32(s.y[i])*Qi[j])
					if quadCoef > 0.0 {
						objDiff = -(gradDiff * gradDiff) / quadCoef
					} else {
						objDiff = -(gradDiff * gradDiff) / TAU
					}
					if objDiff <= objDiffMin {
						GminIdx = j
						objDiffMin = objDiff
					}
				}
			}
		}
	}
	if Gmax+Gmax2 < s.eps || GminIdx == -1 {
		return 1
	}
	workingSet[0] = GmaxIdx
	workingSet[1] = GminIdx
	return 0
}

func (s *Solver) beShrunk(i int, Gmax1, Gmax2 float64) bool {
	if s.isUpperBound(i) {
		if s.y[i] == +1 {
			return -s.G[i] > Gmax1
		} else {
			return -s.G[i] > Gmax2
		}
	} else if s.isLowerBound(i) {
		if s.y[i] == +1 {
			return s.G[i] > Gmax2
		} else {
			return s.G[i] > Gmax1
		}
	} else {
		return false
	}
}

func (s *Solver) doShrinking() {
	var i int
	Gmax1 := -INF // max { -y_i * grad(f)_i | i in I_up(\alpha) }
	Gmax2 := -INF // max { y_i * grad(f)_i | i in I_low(\alpha) }

	// find maximal violating pair first
	// TODO DRY
	for i = 0; i < s.activeSize; i++ {
		if s.y[i] == +1 {
			if !s.isUpperBound(i) {
				if -s.G[i] >= Gmax1 {
					Gmax1 = -s.G[i]
				}
			}
			if !s.isLowerBound(i) {
				if s.G[i] >= Gmax2 {
					Gmax2 = s.G[i]
				}
			}
		} else {
			if !s.isUpperBound(i) {
				if -s.G[i] >= Gmax2 {
					Gmax2 = -s.G[i]
				}
			}
			if !s.isLowerBound(i) {
				if s.G[i] >= Gmax1 {
					Gmax1 = s.G[i]
				}
			}
		}
	}
	if !s.unshrink && Gmax1+Gmax2 <= s.eps*10.0 {
		s.unshrink = true
		s.reconstructGradient()
		s.activeSize = s.l
		log.Print("*")
	}
	for i = 0; i < s.activeSize; i++ {
		if s.beShrunk(i, Gmax1, Gmax2) {
			s.activeSize--
			for s.activeSize > i {
				if !s.beShrunk(s.activeSize, Gmax1, Gmax2) {
					s.swapIndex(i, s.activeSize)
					break
				}
				s.activeSize--
			}
		}
	}
}

func (s *Solver) calculateRho() float64 {
	var r float64
	nrFree := 0
	var ub, lb, sumFree float64 = INF, -INF, 0.0
	for i := 0; i < s.activeSize; i++ {
		yG := float64(s.y[i]) * s.G[i]
		if s.isUpperBound(i) {
			if s.y[i] < 0 {
				ub = math.Min(ub, yG)
			} else {
				lb = math.Max(lb, yG)
			}
		} else if s.isLowerBound(i) {
			if s.y[i] > 0 {
				ub = math.Min(ub, yG)
			} else {
				lb = math.Max(lb, yG)
			}
		} else {
			nrFree++
			sumFree += yG
		}
	}
	if nrFree > 0 {
		r = sumFree / float64(nrFree)
	} else {
		r = (ub + lb) / 2.0
	}
	return r
}

//
// Solver for nu-svm classification and regression
//
// additional constraint: e^T \alpha = constant
//
type SolverNu struct {
	Solver
	si SolutionInfo
}

func (s *SolverNu) Solve(l int, Q QMatrix, p []float64, y []int8, alpha []float64,
	Cp, Cn, eps float64, si *SolutionInfo, shrinking int) {
	s.si = *si
	s.Solve(l, Q, p, y, alpha, Cp, Cn, eps, si, shrinking)
}

func (s *SolverNu) selectWorkingSet(workingSet []int) int {
	// return i,j such that yI = yJ and
	// i: maximizes -yI * grad(f)I, i in IUp(\alpha)
	// j: minimizes the decrease of obj value
	//    (if quadratic coefficeint <= 0, replace it with tau)
	//    -yJ*grad(f)J < -yI*grad(f)I, j in ILow(\alpha)

	Gmaxp := -INF
	Gmaxp2 := -INF
	GmaxpIdx := -1

	Gmaxn := -INF
	Gmaxn2 := -INF
	GmaxnIdx := -1

	GminIdx := -1
	objDiffMin := INF

	for t := 0; t < s.activeSize; t++ {
		if s.y[t] == +1 {
			if !s.isUpperBound(t) {
				if -s.G[t] >= Gmaxp {
					Gmaxp = -s.G[t]
					GmaxpIdx = t
				}
			}
		} else {
			if !s.isLowerBound(t) {
				if s.G[t] >= Gmaxn {
					Gmaxn = s.G[t]
					GmaxnIdx = t
				}
			}
		}
	}
	ip := GmaxpIdx
	in := GmaxnIdx
	var Qip []float32
	var Qin []float32
	if ip != -1 { // NULL Qip not accessed: Gmaxp=-INF if ip=-1
		Qip = s.Q.getQ(ip, s.activeSize)
	}
	if in != -1 {
		Qin = s.Q.getQ(in, s.activeSize)
	}
	for j := 0; j < s.activeSize; j++ {
		// TODO DRY
		if s.y[j] == +1 {
			if !s.isLowerBound(j) {
				gradDiff := Gmaxp + s.G[j]
				if s.G[j] >= Gmaxp2 {
					Gmaxp2 = s.G[j]
				}
				if gradDiff > 0.0 {
					var objDiff float64
					quadCoef := s.QD[ip] + s.QD[j] - float64(2.0*Qip[j])
					if quadCoef > 0.0 {
						objDiff = -(gradDiff * gradDiff) / quadCoef
					} else {
						objDiff = -(gradDiff * gradDiff) / TAU
					}
					if objDiff <= objDiffMin {
						GminIdx = j
						objDiffMin = objDiff
					}
				}
			}
		} else {
			if !s.isUpperBound(j) {
				gradDiff := Gmaxn - s.G[j]
				if -s.G[j] >= Gmaxn2 {
					Gmaxn2 = s.G[j]
				}
				if gradDiff > 0.0 {
					var objDiff float64
					quadCoef := s.QD[in] + s.QD[j] - float64(2.0*Qin[j])
					if quadCoef > 0.0 {
						objDiff = -(gradDiff * gradDiff) / quadCoef
					} else {
						objDiff = -(gradDiff * gradDiff) / TAU
					}
					if objDiff <= objDiffMin {
						GminIdx = j
						objDiffMin = objDiff
					}
				}
			}
		}
	}
	if math.Max(Gmaxp+Gmaxp2, Gmaxn+Gmaxn2) < s.eps || GminIdx == -1 {
		return 1
	}
	if s.y[GminIdx] == +1 {
		workingSet[0] = GmaxpIdx
	} else {
		workingSet[0] = GmaxnIdx
	}
	workingSet[1] = GminIdx
	return 0
}

func (s *SolverNu) beShrunk(i int, Gmax1, Gmax2, Gmax3, Gmax4 float64) bool {
	if s.isUpperBound(i) {
		if s.y[i] == +1 {
			return -s.G[i] > Gmax1
		} else {
			return -s.G[i] > Gmax4
		}
	} else if s.isLowerBound(i) {
		if s.y[i] == +1 {
			return s.G[i] > Gmax2
		} else {
			return s.G[i] > Gmax3
		}
	} else {
		return false
	}
}

func (s *SolverNu) doShrinking() {
	Gmax1 := -INF // max { -y_i * grad(f)_i | y_i = +1, i in I_up(\alpha) }
	Gmax2 := -INF // max { y_i * grad(f)_i | y_i = +1, i in I_low(\alpha) }
	Gmax3 := -INF // max { -y_i * grad(f)_i | y_i = -1, i in I_up(\alpha) }
	Gmax4 := -INF // max { y_i * grad(f)_i | y_i = -1, i in I_low(\alpha) }

	// find maximal violating pair first
	var i int
	for i = 0; i < s.activeSize; i++ {
		if !s.isUpperBound(i) {
			if s.y[i] == +1 {
				if -s.G[i] > Gmax1 {
					Gmax1 = -s.G[i]
				}
			} else if -s.G[i] > Gmax4 {
				Gmax4 = -s.G[i]
			}
		}
		if !s.isLowerBound(i) {
			if s.y[i] == +1 {
				if s.G[i] > Gmax2 {
					Gmax2 = s.G[i]
				}
			} else if s.G[i] > Gmax3 {
				Gmax3 = s.G[i]
			}
		}
	}
	if !s.unshrink && math.Max(Gmax1+Gmax2, Gmax3+Gmax4) <= s.eps {
		s.unshrink = true
		s.reconstructGradient()
		s.activeSize = s.l
	}
	for i = 0; i < s.activeSize; i++ {
		if s.beShrunk(i, Gmax1, Gmax2, Gmax3, Gmax4) {
			s.activeSize--
			for s.activeSize > i {
				if !s.beShrunk(i, Gmax1, Gmax2, Gmax3, Gmax4) {
					s.swapIndex(i, s.activeSize)
					break
				}
				s.activeSize--
			}
		}
	}
}

func (s *SolverNu) calculateRho() float64 {
	var nrFree1, nrFree2 int = 0, 0
	var ub1, ub2 float64 = INF, INF
	var lb1, lb2 float64 = -INF, -INF
	var sumFree1, sumFree2 float64 = 0.0, 0.0

	for i := 0; i < s.activeSize; i++ {
		if s.y[i] == +1 {
			if s.isUpperBound(i) {
				lb1 = math.Max(lb1, s.G[i])
			} else if s.isLowerBound(i) {
				ub1 = math.Min(ub1, s.G[i])
			} else {
				nrFree1++
				sumFree1 += s.G[i]
			}
		} else {
			if s.isUpperBound(i) {
				lb2 = math.Max(lb2, s.G[i])
			} else if s.isLowerBound(i) {
				ub2 = math.Min(ub2, s.G[i])
			} else {
				nrFree2++
				sumFree2 += s.G[i]
			}
		}
	}
	var r1, r2 float64
	if nrFree1 > 0 {
		r1 = sumFree1 / float64(nrFree1)
	} else {
		r1 = (ub1 + lb1) / 2.0
	}
	if nrFree2 > 0 {
		r2 = sumFree2 / float64(nrFree2)
	} else {
		r2 = (ub2 + lb2) / 2.0
	}
	s.si.r = (r1 + r2) / 2.0
	return (r1 - r2) / 2.0
}

//
// Q matrices for various formulations
//
type SvcQ struct {
	Kernel
	y     []int8
	cache *Cache
	QD    []float64
}

func NewSvcQ(prob *svmProblem, param *svmParameter, y []int8) *SvcQ {
	svcq := SvcQ{
		Kernel: *NewKernel(prob.l, prob.x, param),
		y:      make([]int8, len(y)),
		cache:  NewCache(prob.l, int64(param.cacheSize*(1<<20))),
		QD:     make([]float64, prob.l)}

	copy(svcq.y, y)
	for i := 0; i < prob.l; i++ {
		svcq.QD[i] = svcq.kernelFunction(i, i)
	}
	return &svcq
}

func (s *SvcQ) getQ(i, len int) []float32 {
	// TODO make [][]float32 if possible (need to change also Cache method)
	data := make([][]float32, 1)
	var start, j int
	if start = s.cache.getData(i, data, len); start < len {
		for j = start; j < len; j++ {
			data[0][j] = float32(s.y[i]*s.y[j]) * float32(s.kernelFunction(i, j))
		}
	}
	return data[0]
}

func (s *SvcQ) getQD() []float64 {
	return s.QD
}

func (s *SvcQ) swapIndex(i, j int) {
	s.cache.swapIndex(i, j)
	s.swapIndex(i, j)
	s.y[i], s.y[j] = s.y[j], s.y[i]
	s.QD[i], s.QD[j] = s.QD[j], s.QD[i]
}

type OneClassQ struct {
	Kernel
	cache *Cache
	QD    []float64
}

func NewOneClassQ(prob *svmProblem, param *svmParameter) *OneClassQ {
	ocq := OneClassQ{
		Kernel: *NewKernel(prob.l, prob.x, param),
		cache:  NewCache(prob.l, int64(param.cacheSize*(1<<20))),
		QD:     make([]float64, prob.l)}
	for i := 0; i < prob.l; i++ {
		ocq.QD[i] = ocq.kernelFunction(i, i)
	}
	return &ocq
}

func (ocq *OneClassQ) getQ(i, len int) []float32 {
	// TODO make [][]float32 if possible (need to change also Cache method)
	data := make([][]float32, 1)
	var start, j int
	if start = ocq.cache.getData(i, data, len); start < len {
		for j = start; j < len; j++ {
			data[0][j] = float32(ocq.kernelFunction(i, j))
		}
	}
	return data[0]
}

func (ocq *OneClassQ) getQD() []float64 {
	return ocq.QD
}

func (ocq *OneClassQ) swapIndex(i, j int) {
	ocq.cache.swapIndex(i, j)
	ocq.swapIndex(i, j)
	ocq.QD[i], ocq.QD[j] = ocq.QD[j], ocq.QD[i]
}

type SvrQ struct {
	Kernel
	l          int
	cache      *Cache
	sign       []int8
	index      []int
	nextBuffer int
	buffer     [][]float32
	QD         []float64
}

func NewSvrQ(prob *svmProblem, param *svmParameter) *SvrQ {
	svrq := SvrQ{
		Kernel:     *NewKernel(prob.l, prob.x, param),
		l:          prob.l,
		cache:      NewCache(prob.l, int64(param.cacheSize*(1<<20))),
		sign:       make([]int8, 2*prob.l),
		index:      make([]int, 2*prob.l),
		nextBuffer: 0, // If we erase this it still initializes to 0, clarity purposes
		buffer:     make([][]float32, 2),
		QD:         make([]float64, 2*prob.l)}

	for k := 0; k < svrq.l; k++ {
		svrq.sign[k] = 1
		svrq.sign[k+svrq.l] = -1
		svrq.index[k] = k
		svrq.index[k+svrq.l] = k
		svrq.QD[k] = svrq.kernelFunction(k, k)
		svrq.QD[k+svrq.l] = svrq.QD[k]
	}
	svrq.buffer[0] = make([]float32, 2*svrq.l)
	svrq.buffer[1] = make([]float32, 2*svrq.l)
	return &svrq
}

func (s *SvrQ) swapIndex(i, j int) {
	s.sign[i], s.sign[j] = s.sign[j], s.sign[i]
	s.index[i], s.index[j] = s.index[j], s.index[i]
	s.QD[i], s.QD[j] = s.QD[j], s.QD[i]
}

func (s *SvrQ) getQ(i, len int) []float32 {
	// TODO make [][]float32 if possible (need to change also Cache method)
	data := make([][]float32, 1)
	var j int
	realI := s.index[i]
	if s.cache.getData(realI, data, s.l) < len {
		for j = 0; j < s.l; j++ {
			data[0][j] = float32(s.kernelFunction(realI, j))
		}
	}
	// reorder and copy
	buf := s.buffer[s.nextBuffer]
	s.nextBuffer = 1 - s.nextBuffer
	si := s.sign[i]
	for j = 0; j < len; j++ {
		buf[j] = float32(si*s.sign[j]) * data[0][s.index[j]]
	}
	return buf
}

func (s *SvrQ) getQD() []float64 {
	return s.QD
}

//
// construct and solve various formulations
//
func SolveCSvc(prob *svmProblem, param *svmParameter, alpha []float64,
	si *SolutionInfo, Cp, Cn float64) {

	l := prob.l
	minusOnes := make([]float64, l)
	y := make([]int8, l)
	var i int
	for i = 0; i < l; i++ {
		alpha[i] = 0
		minusOnes[i] = -1
		if prob.y[i] > 0 {
			y[i] = +1
		} else {
			y[i] = -1
		}
	}
	s := new(Solver)
	s.Solve(l, NewSvcQ(prob, param, y), minusOnes, y, alpha, Cp, Cn,
		param.eps, si, param.shrinking)
	var sumAlpha float64 = 0.0
	for i = 0; i < l; i++ {
		sumAlpha += alpha[i]
	}
	if Cp == Cn {
		log.Println("nu = %f", sumAlpha/(Cp*float64(prob.l)))
	}
	for i = 0; i < l; i++ {
		alpha[i] *= float64(y[i])
	}
}

func SolveNuSvc(prob *svmProblem, param *svmParameter,
	alpha []float64, si *SolutionInfo) {

	var i int
	l := prob.l
	nu := param.nu
	y := make([]int8, l)

	for i = 0; i < l; i++ {
		if prob.y[i] > 0 {
			y[i] = +1
		} else {
			y[i] = -1
		}
	}

	sumPos := nu * float64(l) / 2.0
	sumNeg := nu * float64(l) / 2.0

	for i = 0; i < l; i++ {
		if y[i] == +1 {
			alpha[i] = math.Min(1.0, sumPos)
			sumPos -= alpha[i]
		} else {
			alpha[i] = math.Min(1.0, sumNeg)
			sumNeg -= alpha[i]
		}
	}
	zeros := make([]float64, l)

	for i = 0; i < l; i++ {
		zeros[i] = 0.0
	}

	s := new(SolverNu)
	s.Solve(l, NewSvcQ(prob, param, y), zeros, y, alpha, 1.0, 1.0,
		param.eps, si, param.shrinking)

	r := si.r
	log.Println("C = %f", 1.0/r)

	for i = 0; i < l; i++ {
		alpha[i] *= float64(y[i]) / r
	}
	si.rho /= r
	si.obj /= r * r
	si.upperBoundP = 1.0 / r
	si.upperBoundN = 1.0 / r
}

func SolveOneClass(prob *svmProblem, param *svmParameter,
	alpha []float64, si *SolutionInfo) {

	l := prob.l
	zeros := make([]float64, l)
	ones := make([]int8, l)
	var i int
	n := int(param.nu) * prob.l // # of alpha's at upper bound

	for i = 0; i < n; i++ {
		alpha[i] = 1
	}
	if n < prob.l {
		alpha[n] = param.nu*float64(prob.l) - float64(n)
	}
	for i = n + 1; i < l; i++ {
		alpha[i] = 0.0
	}
	for i = 0; i < l; i++ {
		zeros[i] = 0.0
		ones[i] = 1
	}
	s := new(Solver)
	s.Solve(l, NewOneClassQ(prob, param), zeros, ones, alpha, 1.0, 1.0,
		param.eps, si, param.shrinking)
}

func SolveEpsilonSvr(prob *svmProblem, param *svmParameter,
	alpha []float64, si *SolutionInfo) {

	l := prob.l
	alpha2 := make([]float64, 2*l)
	linearTerm := make([]float64, 2*l)
	y := make([]int8, 2*l)
	var i int

	for i = 0; i < l; i++ {
		alpha2[i] = 0.0
		linearTerm[i] = param.p - prob.y[i]
		y[i] = 1

		alpha2[i+l] = 0.0
		linearTerm[i+l] = param.p + prob.y[i]
		y[i+l] = -1
	}

	s := new(Solver)
	s.Solve(2*l, NewSvrQ(prob, param), linearTerm, y, alpha2,
		param.C, param.C, param.eps, si, param.shrinking)

	var sumAlpha float64 = 0.0
	for i = 0; i < l; i++ {
		alpha[i] = alpha2[i] - alpha2[i+l]
		sumAlpha += math.Abs(alpha[i])
	}
	log.Println("nu = %f", sumAlpha/(param.C*float64(l)))
}

func SolveNuSvr(prob *svmProblem, param *svmParameter,
	alpha []float64, si *SolutionInfo) {

	l := prob.l
	C := param.C
	alpha2 := make([]float64, 2*l)
	linearTerm := make([]float64, 2*l)
	y := make([]int8, 2*l)
	var i int
	sum := C * param.nu * float64(l) / 2.0

	for i = 0; i < l; i++ {
		alpha2[i] = math.Min(sum, C)
		alpha2[i+l] = math.Min(sum, C)
		sum -= alpha2[i]

		linearTerm[i] = -prob.y[i]
		y[i] = 1

		linearTerm[i+1] = prob.y[i]
		y[i+1] = -1
	}

	s := new(SolverNu)
	s.Solve(2*l, NewSvrQ(prob, param), linearTerm, y, alpha2, C, C,
		param.eps, si, param.shrinking)
	log.Println("epsilon = %f", -si.r)

	for i = 0; i < l; i++ {
		alpha[i] = alpha2[i] - alpha2[i+l]
	}
}

//
// decision_function
//
type decisionFunction struct {
	alpha []float64
	rho   float64
}

func svmTrainOne(prob *svmProblem, param *svmParameter, Cp, Cn float64) *decisionFunction {

	alpha := make([]float64, prob.l)
	var si SolutionInfo
	switch param.svmType {
	case C_SVC:
		SolveCSvc(prob, param, alpha, &si, Cp, Cn)
		break
	case NU_SVC:
		SolveNuSvc(prob, param, alpha, &si)
		break
	case ONE_CLASS:
		SolveOneClass(prob, param, alpha, &si)
		break
	case EPSILON_SVR:
		SolveEpsilonSvr(prob, param, alpha, &si)
		break
	case NU_SVR:
		SolveNuSvr(prob, param, alpha, &si)
		break
	}
	log.Println("obj = %f, rho = %f", si.obj, si.rho)

	// output SVs

	var nSV, nBSV int = 0, 0
	for i := 0; i < prob.l; i++ {
		if math.Abs(alpha[i]) > 0.0 {
			nSV++
			if prob.y[i] > 0 {
				if math.Abs(alpha[i]) >= si.upperBoundP {
					nBSV++
				}
			} else {
				if math.Abs(alpha[i]) >= si.upperBoundN {
					nBSV++
				}
			}
		}
	}
	log.Println("nSV = %d, nBSV = %d", nSV, nBSV)

	return &decisionFunction{alpha: alpha, rho: si.rho}
}

// Platt's binary SVM Probablistic Output: an improvement from Lin et al.
func sigmoidTrain(l int, decValues, labels, probAB []float64) {
	var A, B float64
	var prior1, prior0 float64 = 0.0, 0.0
	var i int

	for i = 0; i < l; i++ {
		if labels[i] > 0.0 {
			prior1 += 1
		} else {
			prior0 += 1
		}
	}

	maxIter := 100 // Maximal number of iterations
	// minStep:  Minimal step taken in line search
	// sigma: For numerically strict PD of Hessian
	var minStep, sigma, eps float64 = 1e-10, 1e-12, 1e-5
	hiTarget := (prior1 + 1.0) / (prior1 + 2.0)
	loTarget := 1.0 / (prior0 + 2.0)
	t := make([]float64, l)
	var fApB, p, q, h11, h22, h21, g1, g2, det, dA, dB, gd, stepsize float64
	var newA, newB, newf, d1, d2 float64
	var iter int

	// Initial Point and Initial Fun Value
	A = 0.0
	B = math.Log((prior0 + 1.0) / (prior1 + 1.0))
	var fval float64 = 0.0

	for i = 0; i < l; i++ {
		if labels[i] > 0.0 {
			t[i] = hiTarget
		} else {
			t[i] = loTarget
		}
		fApB = decValues[i]*A + B
		if fApB >= 0.0 {
			fval += t[i]*fApB + math.Log(1+math.Exp(-fApB))
		} else {
			fval += (t[i]-1)*fApB + math.Log(1+math.Exp(fApB))
		}
	}
	for iter = 0; iter < maxIter; iter++ {
		// Update Gradient and Hessian (use H' = H + sigma I)
		h11, h22 = sigma, sigma // h11: numerically ensures strict PD
		h21, g1, g2 = 0.0, 0.0, 0.0
		for i = 0; i < l; i++ {
			fApB = decValues[i]*A + B
			if fApB >= 0 {
				p = math.Exp(-fApB) / (1.0 + math.Exp(-fApB))
				q = 1.0 / (1.0 + math.Exp(-fApB))
			} else {
				p = 1.0 / (1.0 + math.Exp(fApB))
				q = math.Exp(fApB) / (1.0 + math.Exp(fApB))
			}
			d2 = p * q
			h11 += decValues[i] * decValues[i] * d2
			h22 += d2
			h21 += decValues[i] * d2
			d1 = t[i] - p
			g1 += decValues[i] * d1
			g2 += d1
		}
		// Stopping Criteria
		if math.Abs(g1) < eps && math.Abs(g2) < eps {
			break
		}

		// Finding Newton direction: -inv(H') * g
		det = h11*h22 - h21*h21
		dA = -(h22*g1 - h21*g2) / det
		dB = -(-h21*g1 + h11*g2) / det
		gd = g1*dA + g2*dB

		stepsize = 1.0 // Line Search
		for stepsize >= minStep {
			newA = A + stepsize*dA
			newB = B + stepsize*dB

			// New function value
			newf = 0.0
			for i = 0; i < l; i++ {
				fApB = decValues[i]*newA + newB
				if fApB >= 0 {
					newf += t[i]*fApB + math.Log(1.0+math.Exp(-fApB))
				} else {
					newf += (t[i]-1.0)*fApB + math.Log(1.0+math.Exp(fApB))
				}
			}
			// Check sufficient decrease
			if newf < fval+0.0001*stepsize*gd {
				A, B, fval = newA, newB, newf
				break
			} else {
				stepsize /= 2.0
			}
		}
		if stepsize < minStep {
			log.Println("Line search fails in two-class probability estimates")
			break
		}
	}
	if iter >= maxIter {
		log.Println("Reaching maximal iterations in two-class probability estimates")
	}
	probAB[0], probAB[1] = A, B
}

func sigmoidPredict(decisionValue, A, B float64) float64 {
	fApB := decisionValue*A + B
	// 1-p used later; avoid catastrophic cancellation
	if fApB >= 0 {
		return math.Exp(-fApB) / (1.0 + math.Exp(-fApB))
	} else {
		return 1.0 / (1.0 + math.Exp(fApB))
	}
}

// Method 2 from the multiclass_prob paper by Wu, Lin, and Weng
func multiclassProbability(k int, r [][]float64, p []float64) {
	var t, j int
	iter, maxIter := 0, int(math.Max(100.0, float64(k)))
	Q := make([][]float64, k)
	Qp := make([]float64, k)
	var pQp float64
	eps := 0.005 / float64(k)

	for t = 0; t < k; t++ {
		p[t] = 1.0 / float64(k) // Valid if k = 1
		Q[t] = make([]float64, k)
		Q[t][t] = 0.0
		for j = 0; j < t; j++ {
			Q[t][t] += r[j][t] * r[j][t]
			Q[t][j] = Q[j][t]
		}
		for j = t + 1; j < k; j++ {
			Q[t][t] += r[j][t] * r[j][t]
			Q[t][j] = -r[j][t] * r[t][j]
		}
	}
	for iter = 0; iter < maxIter; iter++ {
		// stopping condition, recalculate QP,pQP for numerical accuracy
		pQp = 0.0
		for t = 0; t < k; t++ {
			Qp[t] = 0
			for j = 0; j < k; j++ {
				Qp[t] += Q[t][j] * p[j]
			}
			pQp += p[t] * Qp[t]
		}
		var maxError float64 = 0.0
		for t = 0; t < k; t++ {
			if iterError := math.Abs(Qp[t] - pQp); iterError > maxError {
				maxError = iterError
			}
		}
		if maxError > eps {
			break
		}
		for t = 0; t < k; t++ {
			diff := (-Qp[t] + pQp) / Q[t][t]
			p[t] += diff
			pQp = (pQp + diff*(diff*Q[t][t]+2.0*Qp[t])) / (1.0 + diff) / (1.0 + diff)
			for j = 0; j < k; j++ {
				Qp[j] = (Qp[j] + diff*Q[t][j]) / (1.0 + diff)
				p[j] /= (1.0 + diff)
			}
		}
	}
	if iter >= maxIter {
		log.Println("Exceeds max_iter in multiclass_prob")
	}
}

// Cross-validation decision values for probability estimate
func svmBinarySvcProbability(prob *svmProblem, param *svmParameter,
	Cp, Cn float64, probAB []float64) {

	var i int
	nrFold := 5
	perm := make([]int, prob.l)
	decValues := make([]float64, prob.l)

	// random shuffle
	for i = 0; i < prob.l; i++ {
		perm[i] = i
	}
	for i = 0; i < prob.l; i++ {
		j := i + int(rand.Int31n(int32(prob.l-i)))
		perm[i], perm[j] = perm[j], perm[i]
	}
	for i = 0; i < nrFold; i++ {
		begin := int(i * prob.l / nrFold)
		end := int((i + 1) * prob.l / nrFold)
		var j, k int
		// TODO make var for l and change name if possible to length
		subprob := svmProblem{
			l: prob.l - (end - begin),
			x: make([][]svmNode, prob.l-(end-begin)),
			y: make([]float64, prob.l-(end-begin))}

		k = 0
		// TODO DRY
		for j = 0; j < begin; j++ {
			subprob.x[k] = prob.x[perm[j]]
			subprob.y[k] = prob.y[perm[j]]
			k++
		}
		for j = end; j < prob.l; j++ {
			subprob.x[k] = prob.x[perm[j]]
			subprob.y[k] = prob.y[perm[j]]
			k++
		}
		pCount, nCount := 0, 0
		for j = 0; j < k; j++ {
			if subprob.y[j] > 0 {
				pCount++
			} else {
				nCount++
			}
		}
		if pCount == 0 && nCount == 0 {
			for j = begin; j < end; j++ {
				decValues[perm[j]] = 0.0
			}
		} else if pCount > 0 && nCount == 0 {
			for j = begin; j < end; j++ {
				decValues[perm[j]] = 1.0
			}
		} else if pCount == 0 && nCount > 0 {
			for j = begin; j < end; j++ {
				decValues[perm[j]] = -1.0
			}
		} else {
			subparam := param
			subparam.probability = 0
			subparam.C = 1.0
			subparam.nrWeight = 2
			subparam.weightLabel = make([]int, 2)
			subparam.weight = make([]float64, 2)
			subparam.weightLabel[0], subparam.weightLabel[1] = +1, -1
			subparam.weight[0], subparam.weight[1] = Cp, Cn
			submodel := svmTrain(&subprob, subparam)
			for j = begin; j < end; j++ {
				decValue := make([]float64, 1)
				svmPredictValues(submodel, prob.x[perm[j]], decValue)
				decValues[perm[j]] = decValue[0]
				// ensure +1 -1 order; reason not using CV subroutine
				decValues[perm[j]] *= float64(submodel.label[0])
			}
		}
	}
	sigmoidTrain(prob.l, decValues, prob.y, probAB)
}

func svmSvrProbability(prob *svmProblem, param *svmParameter) float64 {
	var i int
	nrFold := 5
	ymv := make([]float64, prob.l)
	var mae float64 = 0.0

	newparam := param
	newparam.probability = 0
	svmCrossValidation(prob, newparam, nrFold, ymv)
	for i = 0; i < prob.l; i++ {
		ymv[i] = prob.y[i] - ymv[i]
		mae += math.Abs(ymv[i])
	}
	mae /= float64(prob.l)
	std := math.Sqrt(2.0 * mae * mae)
	count := 0
	mae = 0.0
	for i = 0; i < prob.l; i++ {
		if math.Abs(ymv[i]) > 5*std {
			count++
		} else {
			mae += math.Abs(ymv[i])
		}
	}
	mae /= float64(prob.l - count)
	log.Println("Prob. model for test data: target value = predicted value + z,\nz: Laplace distribution e^(-|z|/sigma)/(2sigma),sigma= %g", mae)
	return mae
}

// label: label name, start: begin of each class, count: #data of classes, perm: indices to the original data
// perm, length l, must be allocated before calling this subroutine
func svmGroupClasses(prob *svmProblem, nrClassRet []int,
	labelRet, startRet, countRet [][]int, perm []int) {

	l := prob.l
	maxNrClass := 16
	nrClass := 0
	label := make([]int, maxNrClass)
	count := make([]int, maxNrClass)
	dataLabel := make([]int, l)
	var i int
	for i = 0; i < l; i++ {
		thisLabel := int(prob.y[i])
		var j int
		for j = 0; j < nrClass; j++ {
			if thisLabel == label[j] {
				count[j]++
				break
			}
		}
		dataLabel[i] = j
		if j == nrClass {
			if nrClass == maxNrClass {
				maxNrClass *= 2
				labelCopy := make([]int, maxNrClass)
				copy(labelCopy, label)
				label = labelCopy
				countCopy := make([]int, maxNrClass)
				copy(countCopy, count)
				count = countCopy
			}
			label[nrClass] = thisLabel
			count[nrClass] = 1
			nrClass++
		}
	}
	//
	// Labels are ordered by their first occurrence in the training set.
	// However, for two-class sets with -1/+1 labels and -1 appears first,
	// we swap labels to ensure that internally the binary SVM has positive data corresponding to the +1 instances.
	//
	if nrClass == 2 && label[0] == -1 && label[1] == 1 {
		label[0], label[1] = label[1], label[0]
		count[0], count[1] = count[1], count[0]
		for i = 0; i < l; i++ {
			if dataLabel[i] == 0 {
				dataLabel[i] = 1
			} else {
				dataLabel[i] = 0
			}
		}
	}
	start := make([]int, nrClass)
	start[0] = 0
	for i = 1; i < nrClass; i++ {
		start[i] = start[i-1] + count[i-1]
	}
	for i = 0; i < l; i++ {
		perm[start[dataLabel[i]]] = i
		start[dataLabel[i]]++
	}
	start[0] = 0
	for i = 1; i < nrClass; i++ {
		start[i] = start[i-1] + count[i-1]
	}
	nrClassRet[0] = nrClass
	labelRet[0] = label
	startRet[0] = start
	countRet[0] = count
}

//
// Interface functions
//
func svmTrain(prob *svmProblem, param *svmParameter) *svmModel {
	model := svmModel{param: param}

	if param.svmType == ONE_CLASS ||
		param.svmType == EPSILON_SVR ||
		param.svmType == NU_SVR {

		// regression or one-class-svm
		model.nrClass = 2
		model.label = nil
		model.nSV = nil
		model.probA, model.probB = nil, nil
		model.svCoef = make([][]float64, 1)

		if param.probability == 1 &&
			param.svmType == EPSILON_SVR ||
			param.svmType == NU_SVR {

			model.probA = make([]float64, 1)
			model.probA[0] = svmSvrProbability(prob, param)
		}
		f := svmTrainOne(prob, param, 0.0, 0.0)
		model.rho = make([]float64, 1)
		model.rho[0] = f.rho

		nSV := 0
		var i int
		for i = 0; i < prob.l; i++ {
			if math.Abs(f.alpha[i]) > 0 {
				nSV++
			}
		}
		model.l = nSV
		model.SV = make([][]svmNode, nSV)
		model.svCoef[0] = make([]float64, nSV)
		model.svIndices = make([]int, nSV)
		j := 0
		for i = 0; i < prob.l; i++ {
			if math.Abs(f.alpha[i]) > 0 {
				model.SV[j] = prob.x[i]
				model.svCoef[0][j] = f.alpha[i]
				model.svIndices[j] = i + 1
				j++
			}
		}
	} else {
		// classification
		l := prob.l
		tmpNrClass := make([]int, 1)
		tmpLabel := make([][]int, 1)
		tmpStart := make([][]int, 1)
		tmpCount := make([][]int, 1)
		perm := make([]int, l)

		// group training data of the same class
		svmGroupClasses(prob, tmpNrClass, tmpLabel, tmpStart, tmpCount, perm)
		nrClass := tmpNrClass[0]
		label := tmpLabel[0]
		start := tmpStart[0]
		count := tmpCount[0]

		if nrClass == 1 {
			log.Println("WARNING: training data in only one class. See README for details.")
		}

		x := make([][]svmNode, l)
		var i int
		for i = 0; i < l; i++ {
			x[i] = prob.x[perm[i]]
		}
		// calculate weighted C
		weightedC := make([]float64, nrClass)
		for i = 0; i < nrClass; i++ {
			weightedC[i] = param.C
		}
		for i = 0; i < param.nrWeight; i++ {
			var j int
			for j = 0; j < nrClass; j++ {
				if param.weightLabel[i] == label[j] {
					break
				}
			}
			if j == nrClass {
				log.Printf("WARNING: class label %d specified in weight is not found\n", param.weightLabel[i])
			} else {
				weightedC[j] *= param.weight[i]
			}
		}
		// train k*(k-1)/2 models
		nonzero := make([]bool, l) // Initialized by default to false
		f := make([]decisionFunction, nrClass*(nrClass-1)/2)

		var probA, probB []float64
		probA, probB = nil, nil
		if param.probability == 1 {
			probA = make([]float64, nrClass*(nrClass-1)/2)
			probB = make([]float64, nrClass*(nrClass-1)/2)
		}

		p := 0
		for i = 0; i < nrClass; i++ {
			for j := i + 1; j < nrClass; j++ {
				subProb := new(svmProblem)
				si, sj := start[i], start[j]
				ci, cj := count[i], count[j]
				subProb.l = ci + cj
				subProb.x = make([][]svmNode, subProb.l)
				subProb.y = make([]float64, subProb.l)
				var k int
				for k = 0; k < ci; k++ {
					subProb.x[k] = x[si+k]
					subProb.y[k] = +1
				}
				for k = 0; k < cj; k++ {
					subProb.x[ci+k] = x[sj+k]
					subProb.y[ci+k] = -1
				}
				if param.probability == 1 {
					// TODO try to pass references as in c impl
					probAB := make([]float64, 2)
					svmBinarySvcProbability(subProb, param,
						weightedC[i], weightedC[j], probAB)
					probA[p] = probAB[0]
					probB[p] = probAB[1]
				}
				f[p] = *svmTrainOne(subProb, param, weightedC[i], weightedC[j])
				for k = 0; k < ci; k++ {
					if !nonzero[si+k] && math.Abs(f[p].alpha[k]) > 0.0 {
						nonzero[si+k] = true
					}
				}
				for k = 0; k < cj; k++ {
					if !nonzero[sj+k] && math.Abs(f[p].alpha[ci+k]) > 0.0 {
						nonzero[sj+k] = true
					}
				}
				p++
			}
		}
		// build output
		model.nrClass = nrClass
		model.label = make([]int, nrClass)
		for i = 0; i < nrClass; i++ {
			model.label[i] = label[i]
		}

		model.rho = make([]float64, nrClass*(nrClass-1)/2)
		// TODO change for range
		for i = 0; i < nrClass*(nrClass-1)/2; i++ {
			model.rho[i] = f[i].rho
		}
		// TODO check if param.probability is used as boolean
		if param.probability == 1 {
			model.probA = make([]float64, nrClass*(nrClass-1)/2)
			model.probB = make([]float64, nrClass*(nrClass-1)/2)
			for i = 0; i < nrClass*(nrClass-1)/2; i++ {
				model.probA[i] = probA[i]
				model.probB[i] = probB[i]
			}
		} else {
			model.probA, model.probB = nil, nil
		}
		totalSv := 0
		nzCount := make([]int, nrClass)
		model.nSV = make([]int, nrClass)
		for i = 0; i < nrClass; i++ {
			nSV := 0
			for j := 0; j < count[i]; j++ {
				if nonzero[start[i]+j] {
					nSV++
					totalSv++
				}
			}
			model.nSV[i] = nSV
			nzCount[i] = nSV
		}
		log.Println("Total nSV = %d\n", totalSv)

		model.l = totalSv
		model.SV = make([][]svmNode, totalSv)
		model.svIndices = make([]int, totalSv)
		p = 0
		for i = 0; i < l; i++ {
			if nonzero[i] {
				model.SV[p] = x[i]
				model.svIndices[p] = perm[i] + 1
				p++
			}
		}
		nzStart := make([]int, totalSv)
		nzStart[0] = 0
		for i = 1; i < nrClass; i++ {
			nzStart[i] = nzStart[i-1] + nzCount[i-1]
		}
		model.svCoef = make([][]float64, nrClass-1)
		for i = 0; i < nrClass-1; i++ {
			model.svCoef[i] = make([]float64, totalSv)
		}
		p = 0
		for i = 0; i < nrClass; i++ {
			for j := i + 1; j < nrClass; j++ {
				// classifier (i,j): coefficients with
				// i are in sv_coef[j-1][nz_start[i]...],
				// j are in sv_coef[i][nz_start[j]...]

				si, sj := start[i], start[j]
				ci, cj := count[i], count[j]
				q := nzStart[i]
				var k int
				for k = 0; k < ci; k++ {
					if nonzero[si+k] {
						model.svCoef[j-1][q] = f[p].alpha[k]
						q++
					}
				}
				q = nzStart[i]
				for k = 0; k < cj; k++ {
					if nonzero[sj+k] {
						model.svCoef[i][q] = f[p].alpha[ci+k]
						q++
					}
				}
				p++
			}
		}
	}
	return &model
}

// Stratified cross validation
func svmCrossValidation(prob *svmProblem, param *svmParameter, nrFold int, target []float64) {
	var i int
	var foldStart []int
	l := prob.l
	perm := make([]int, l)
	if nrFold > l {
		nrFold = l
		log.Println("WARNING: # folds > # data. Will use # folds = # data instead (i.e., leave-one-out cross validation)")
	}
	foldStart = make([]int, nrFold+1)
	// stratified cv may not give leave-one-out rate
	// Each class to l folds -> some folds may have zero elements
	if param.svmType == C_SVC || param.svmType == NU_SVC && nrFold < l {
		tmpNrClass := make([]int, 1)
		tmpLabel := make([][]int, 1)
		tmpStart := make([][]int, 1)
		tmpCount := make([][]int, 1)

		svmGroupClasses(prob, tmpNrClass, tmpLabel, tmpStart, tmpCount, perm)

		nrClass := tmpNrClass[0]
		start := tmpStart[0]
		count := tmpCount[0]

		// random shuffle and then data grouped by fold using the array perm
		foldCount := make([]int, nrFold)
		var c int
		index := make([]int, l)
		for i = 0; i < l; i++ {
			index[i] = perm[i]
		}
		for c = 0; c < nrClass; c++ {
			for i = 0; i < l; i++ {
				j := i + int(rand.Int31n(int32(count[c]-i)))
				index[start[c]+j], index[start[c]+i] = index[start[c]+i], index[start[c]+j]
			}
		}
		for i = 0; i < nrFold; i++ {
			foldCount[i] = 0
			for c = 0; c < nrClass; c++ {
				foldCount[i] += (i+1)*count[c]/nrFold - i*count[c]/nrFold
			}
		}
		foldStart[0] = 0
		for i = 1; i <= nrFold; i++ {
			foldStart[i] = foldStart[i-1] + foldCount[i-1]
		}
		for c = 0; c < nrClass; c++ {
			for i = 0; i < nrFold; i++ {
				begin := start[c] + i*count[c]/nrFold
				end := start[c] + (i+1)*count[c]/nrFold
				for j := begin; j < end; j++ {
					perm[foldStart[i]] = index[j]
					foldStart[i]++
				}
			}
		}
		foldStart[0] = 0
		for i = 1; i <= nrFold; i++ {
			foldStart[i] = foldStart[i-1] + foldCount[i-1]
		}

	} else {
		for i = 0; i < l; i++ {
			perm[i] = i
		}
		for i = 0; i < l; i++ {
			j := i + int(rand.Int31n(int32(l-i)))
			perm[i], perm[j] = perm[j], perm[i]
		}
		for i = 0; i < nrFold; i++ {
			foldStart[i] = i * l / nrFold
		}
	}
	for i = 0; i < nrFold; i++ {
		begin := foldStart[i]
		end := foldStart[i+1]
		var j, k int
		subprob := svmProblem{
			l: l - (end - begin),
			x: make([][]svmNode, l-(end-begin)),
			y: make([]float64, l-(end-begin))}
		k = 0
		for j = 0; j < begin; j++ {
			subprob.x[k] = prob.x[perm[j]]
			subprob.y[k] = prob.y[perm[j]]
			k++
		}
		for j = end; j < l; j++ {
			subprob.x[k] = prob.x[perm[j]]
			subprob.y[k] = prob.y[perm[j]]
			k++
		}
		submodel := svmTrain(&subprob, param)
		if param.probability == 1 && (param.svmType == C_SVC || param.svmType == NU_SVC) {
			probEstimates := make([]float64, svmGetNrClass(submodel))
			for j = begin; j < end; j++ {
				target[perm[j]] = svmPredictProbability(submodel, prob.x[perm[j]], probEstimates)
			}
		} else {
			for j = begin; j < end; j++ {
				target[perm[j]] = svmPredict(submodel, prob.x[perm[j]])
			}
		}
	}
}

func svmGetSvmType(model *svmModel) int {
	return model.param.svmType
}

func svmGetNrClass(model *svmModel) int {
	return model.nrClass
}

func svmGetLabels(model *svmModel, label []int) {
	if model.label != nil {
		for i := 0; i < model.nrClass; i++ {
			label[i] = model.label[i]
		}
	}
}

func svmGetSvIndices(model *svmModel, indices []int) {
	if model.svIndices != nil {
		//change to copy(indices, model.svIndices)
		for i := 0; i < model.l; i++ {
			indices[i] = model.svIndices[i]
		}
	}
}

func svmGetNrSv(model *svmModel) int {
	return model.l
}

func svmGetSvrProbability(model *svmModel) float64 {
	if (model.param.svmType == EPSILON_SVR || model.param.svmType == NU_SVR) &&
		model.probA != nil {
		return model.probA[0]
	} else {
		log.Println("Model doesn't contain information for SVR probability inference")
		return 0.0
	}
}

func svmPredictValues(model *svmModel, x []svmNode, decValues []float64) float64 {
	var i int
	if model.param.svmType == ONE_CLASS ||
		model.param.svmType == EPSILON_SVR ||
		model.param.svmType == NU_SVR {

		svCoef := model.svCoef[0]
		sum := float64(0)
		for i = 0; i < model.l; i++ {
			sum += svCoef[i] * kFunction(x, model.SV[i], model.param)
		}
		sum -= model.rho[0]
		decValues[0] = sum

		if model.param.svmType == ONE_CLASS {
			if sum > 0.0 {
				return 1
			} else {
				return -1
			}
		} else {
			return sum
		}
	} else {
		nrClass := model.nrClass
		l := model.l

		kvalue := make([]float64, l)
		for i = 0; i < l; i++ {
			kvalue[i] = kFunction(x, model.SV[i], model.param)
		}
		start := make([]int, nrClass)
		start[0] = 0 // already initialized to 0 (Golang default beahviour)
		for i = 1; i < nrClass; i++ {
			start[i] = start[i-1] + model.nSV[i-1]
		}

		vote := make([]int, nrClass)
		for i = 0; i < nrClass; i++ { // TODO Can be erased, already 0 values
			vote[i] = 0
		}
		p := 0
		for i = 0; i < nrClass; i++ {
			for j := i + 1; j < nrClass; j++ {
				sum := float64(0.0)
				si, sj := start[i], start[j]
				ci, cj := model.nSV[i], model.nSV[j]

				var k int
				coef1 := model.svCoef[j-1]
				coef2 := model.svCoef[i]
				for k = 0; k < ci; k++ {
					sum += coef1[si+k] * kvalue[si+k]
				}
				for k = 0; k < cj; k++ {
					sum += coef2[sj+k] * kvalue[sj+k]
				}
				sum -= model.rho[p]
				decValues[p] = sum

				if decValues[p] > 0 {
					vote[i]++
				} else {
					vote[j]++
				}
				p++
			}
		}
		voteMaxIdx := 0
		for i = 1; i < nrClass; i++ {
			if vote[i] > vote[voteMaxIdx] {
				voteMaxIdx = i
			}
		}
		return float64(model.label[voteMaxIdx])
	}
}

func svmPredict(model *svmModel, x []svmNode) float64 {
	nrClass := model.nrClass
	var decValues []float64
	if model.param.svmType == ONE_CLASS ||
		model.param.svmType == EPSILON_SVR ||
		model.param.svmType == NU_SVR {

		decValues = make([]float64, 1)
	} else {
		decValues = make([]float64, nrClass*(nrClass-1)/2)
	}
	predResult := svmPredictValues(model, x, decValues)
	return predResult
}

func svmPredictProbability(model *svmModel, x []svmNode, probEstimates []float64) float64 {

	if (model.param.svmType == C_SVC || model.param.svmType == NU_SVC) &&
		model.probA != nil && model.probB != nil {

		var i int
		nrClass := model.nrClass
		decValues := make([]float64, nrClass/(nrClass-1)/2)
		svmPredictValues(model, x, decValues)

		minProb := float64(1e-7)
		pairwiseProb := make([][]float64, nrClass)
		for i = 0; i < nrClass; i++ {
			pairwiseProb[i] = make([]float64, nrClass)
		}
		k := 0
		for i = 0; i < nrClass; i++ {
			for j := i + 1; j < nrClass; j++ {
				pairwiseProb[i][j] = math.Min(math.Max(
					sigmoidPredict(decValues[k], model.probA[k], model.probB[k]),
					minProb), 1-minProb)
				pairwiseProb[j][i] = 1 - pairwiseProb[i][j]
				k++
			}
		}
		if nrClass == 2 {
			probEstimates[0] = pairwiseProb[0][1]
			probEstimates[1] = pairwiseProb[1][0]
		} else {
			multiclassProbability(nrClass, pairwiseProb, probEstimates)
		}
		probMaxIdx := 0
		for i = 1; i < nrClass; i++ {
			if probEstimates[i] > probEstimates[probMaxIdx] {
				probMaxIdx = i
			}
		}
		return float64(model.label[probMaxIdx])
	} else {
		return svmPredict(model, x)
	}
}

var (
	svmTypeTable    = [...]string{"c_svc", "nu_svc", "one_class", "epsilon_svr", "nu_svr"}
	kernelTypeTable = [...]string{"linear", "polynomial", "rbf", "sigmoid", "precomputed"}
)

func svmSaveModel(modelFileName string, model *svmModel) int {
	fp, err := os.OpenFile(modelFileName, os.O_WRONLY|os.O_CREATE|os.O_SYNC, 0644)
	if err != nil {
		log.Fatal(err)
		return -1
	}
	param := model.param
	// TODO Use function to simplyfy writes writeFile(format, ...args)
	// TODO check bytes read, err
	fp.WriteString(fmt.Sprintf("svm_type %s\n", svmTypeTable[param.svmType]))
	fp.WriteString(fmt.Sprintf("kernel_type %s\n", kernelTypeTable[param.kernelType]))

	if param.kernelType == POLY {
		fp.WriteString(fmt.Sprintf("degree %d\n", param.degree))
	}
	if param.kernelType == POLY ||
		param.kernelType == RBF ||
		param.kernelType == SIGMOID {

		fp.WriteString(fmt.Sprintf("gamma %g\n", param.gamma))
	}
	if param.kernelType == POLY || param.kernelType == SIGMOID {
		fp.WriteString(fmt.Sprintf("coef0 %g\n", param.coef0))
	}
	nrClass := model.nrClass
	l := model.l
	fp.WriteString(fmt.Sprintf("nr_class %d\n", nrClass))
	fp.WriteString(fmt.Sprintf("total_sv %d\n", l))

	{
		fp.WriteString("rho")
		for i := 0; i < nrClass*(nrClass-1)/2; i++ {
			fp.WriteString(fmt.Sprintf(" %g", model.rho[i]))
		}
		fp.WriteString("\n")
	}
	if model.label != nil {
		fp.WriteString("label")
		for i := 0; i < nrClass; i++ {
			fp.WriteString(fmt.Sprintf(" %d", model.label[i]))
		}
		fp.WriteString("\n")
	}
	if model.probA != nil { // regression has probA only
		fp.WriteString("probA")
		for i := 0; i < nrClass*(nrClass-1)/2; i++ {
			fp.WriteString(fmt.Sprintf(" %g", model.probA[i]))
		}
		fp.WriteString("\n")
	}
	if model.probB != nil {
		fp.WriteString("probB")
		for i := 0; i < nrClass*(nrClass-1)/2; i++ {
			fp.WriteString(fmt.Sprintf(" %g", model.probB[i]))
		}
		fp.WriteString("\n")
	}
	if model.nSV != nil {
		fp.WriteString("nr_Sv")
		for i := 0; i < nrClass; i++ {
			fp.WriteString(fmt.Sprintf(" %d", model.nSV[i]))
		}
		fp.WriteString("\n")
	}
	fp.WriteString("SV\n")
	svCoef := model.svCoef
	SV := model.SV
	for i := 0; i < l; i++ {
		for j := 0; j < nrClass; j++ {
			fp.WriteString(fmt.Sprintf(" %.16g ", svCoef[j][i]))
		}
		p := SV[i]
		if param.kernelType == PRECOMPUTED {
			fp.WriteString(fmt.Sprintf("0:%d ", int(p[0].value)))
		} else {
			for j := range p {
				fp.WriteString(fmt.Sprintf("%d:%.8g ", p[j].index, p[j].value))
			}
		}
		fp.WriteString("\n")
	}
	if err := fp.Close(); err != nil {
		log.Fatal(err)
		return -1
	}
	return 0
}

// TODO improvements
func readModelHeader(r io.Reader, model *svmModel) bool {
	param := new(svmParameter)
	model.param = param
	// parameters for training only won't be assigned, but arrays are assigned as NULL for safety
	// Can be ommited -> zero values in go
	param.nrWeight = 0
	param.weightLabel = nil
	param.weight = nil

	var cmd string
	for {
		n, err := fmt.Fscanf(r, "%80s", &cmd)
		if n == 0 || err != nil {
			return false
		}
		switch cmd {
		case "svm_type":
			n, err := fmt.Fscanf(r, "%80s", &cmd)
			if n == 0 || err != nil {
				return false
			}
			i := -1
			for k := range svmTypeTable {
				if cmd == svmTypeTable[k] {
					param.svmType = k
					i = k
					break
				}
			}
			if i != -1 {
				// TODO Change to log.Fatal or alike?
				log.Println("Unknown svm type.")
				return false
			}
			break
		case "kernel_type":
			n, err := fmt.Fscanf(r, "%80s", &cmd)
			if n == 0 || err != nil {
				return false
			}
			i := -1
			for k := range kernelTypeTable {
				if cmd == kernelTypeTable[k] {
					param.kernelType = k
					i = k
					break
				}
			}
			if i != -1 {
				// TODO Change to log.Fatal or alike?
				log.Println("Unknown kernel function.")
				return false
			}
			break
		case "degree":
			n, err := fmt.Fscanf(r, "%d", &param.degree)
			if n == 0 || err != nil {
				return false
			}
			break
		case "gamma":
			n, err := fmt.Fscanf(r, "%.6f", &param.gamma)
			if n == 0 || err != nil {
				return false
			}
			break
		case "coef0":
			n, err := fmt.Fscanf(r, "%.6f", &param.coef0)
			if n == 0 || err != nil {
				return false
			}
			break
		case "nr_class":
			n, err := fmt.Fscanf(r, "%d", &model.nrClass)
			if n == 0 || err != nil {
				return false
			}
			break
		case "total_sv":
			n, err := fmt.Fscanf(r, "%d", &model.l)
			if n == 0 || err != nil {
				return false
			}
			break
		case "rho":
			n := model.nrClass * (model.nrClass - 1) / 2
			model.rho = make([]float64, n)
			for i := 0; i < n; i++ {
				bytesRead, err := fmt.Fscanf(r, "%.6f", &model.rho[i])
				if bytesRead == 0 || err != nil {
					return false
				}
			}
			break
		case "label":
			n := model.nrClass
			model.label = make([]int, n)
			for i := 0; i < n; i++ {
				bytesRead, err := fmt.Fscanf(r, "%d", &model.label[i])
				if bytesRead == 0 || err != nil {
					return false
				}
			}
			break
		case "probA":
			n := model.nrClass * (model.nrClass - 1) / 2
			model.probA = make([]float64, n)
			for i := 0; i < n; i++ {
				bytesRead, err := fmt.Fscanf(r, "%.6f", &model.probA[i])
				if bytesRead == 0 || err != nil {
					return false
				}
			}
			break
		case "probB":
			n := model.nrClass * (model.nrClass - 1) / 2
			model.probB = make([]float64, n)
			for i := 0; i < n; i++ {
				bytesRead, err := fmt.Fscanf(r, "%.6f", &model.probB[i])
				if bytesRead == 0 || err != nil {
					return false
				}
			}
			break
		case "nr_sv":
			n := model.nrClass
			model.nSV = make([]int, n)
			for i := 0; i < n; i++ {
				bytesRead, err := fmt.Fscanf(r, "%d", &model.nSV[i])
				if bytesRead == 0 || err != nil {
					return false
				}
			}
			break
		case "SV":
			// for {
			// 	var c int
			// 	if c == EOF || c == '\n' {
			// 		break
			// 	}
			// }
			break
		default:
			log.Printf("unknown text in model file: [%s]\n", cmd)
			return false
		}
	}
	return true
}

func mySplit(r rune) bool {
	switch r {
	case ' ', '\t', '\n', '\r', '\f', ':':
		return true
	}
	return false
}

func svmLoadModel(modelFileName string) (model *svmModel) {
	fp, err := os.Open(modelFileName)
	if err != nil {
		log.Fatal(err)
		return nil
	}
	// read parameters
	// read header
	if !readModelHeader(fp, model) {
		log.Fatal("ERROR: failed to read model\n")
		return nil
	}
	// read sv_coef and SV
	elements := 0
	// pos := seek of fp
	// read elements
	elements += model.l
	// return to pos of fp

	m := model.nrClass - 1
	l := model.l
	model.svCoef = make([][]float64, m)
	var i int
	for i = 0; i < m; i++ {
		model.svCoef[i] = make([]float64, l)
	}
	model.SV = make([][]svmNode, l)
	// var xSpace []svmNode
	// if l > 0 {
	// 	xSpace = make([]svmNode, elements)
	// }
        // j := 0
	scanner := bufio.NewScanner(fp)
	for i = 0; i < l; i++ {
		scanner.Scan()
		line := strings.FieldsFunc(scanner.Text(), mySplit)

		for k := 0; k < m; k++ {
			coef, err := strconv.ParseFloat(line[k], 64)
			if err != nil {
				log.Fatal("conversion error")
			}
			model.svCoef[k][i] = coef
		}
		n := int((len(line) - m) / 2)
		model.SV[i] = make([]svmNode, n)
		for j := 0; j < n; j++ {
			index, err := strconv.Atoi(line[m+j])
			if err != nil {
				log.Fatal("error parsing")
			}
			value, err := strconv.ParseFloat(line[m+j+1], 64)
			if err != nil {
				log.Fatal("Error parsing")
			}
			model.SV[i][j] = svmNode{index: index, value: value}
		}
	}
	if err := fp.Close(); err != nil {
		log.Fatal(err)
		return nil
	}
	return
}

func svmCheckParameter(prob *svmProblem, param svmParameter) string {
	// svm_type
	svmType := param.svmType
	if svmType != C_SVC && svmType != NU_SVC && svmType != ONE_CLASS &&
		svmType != EPSILON_SVR && svmType != NU_SVR {
		return "unknown svm type"
	}
	// kernel_type, degree
	kernelType := param.kernelType
	if kernelType != LINEAR && kernelType != POLY && kernelType != RBF &&
		kernelType != SIGMOID && kernelType != PRECOMPUTED {
		return "unknown kernel type"
	}
	if param.gamma < 0 {
		return "gamma < 0"
	}
	if param.degree < 0 {
		return "degree of polynomial kernel < 0"
	}
	// cache_size,eps,C,nu,p,shrinking
	if param.cacheSize <= 0 {
		return "cache_size <= 0"
	}
	if param.eps <= 0 {
		return "eps <= 0"
	}
	if svmType == C_SVC || svmType == EPSILON_SVR || svmType == NU_SVR {
		if param.C <= 0 {
			return "C <= 0"
		}
	}
	if svmType == NU_SVC || svmType == ONE_CLASS || svmType == NU_SVR {
		if param.nu <= 0 || param.nu > 1 {
			return "nu <= 0 or nu > 1"
		}
	}
	if svmType == EPSILON_SVR {
		if param.p < 0 {
			return "p < 0"
		}
	}
	if param.shrinking != 0 && param.shrinking != 1 {
		return "shrinking != 0 and shrinking != 1"
	}
	if param.probability != 0 && param.probability != 1 {
		return "probability != 0 and probability != 1"
	}
	if param.probability == 1 && svmType == ONE_CLASS {
		return "one-class SVM probability output not supported yet"
	}
	// check whether nu-svc is feasible
	if svmType == NU_SVC {
		l := prob.l
		maxNrClass := 16
		nrClass := 0
		label := make([]int, maxNrClass)
		count := make([]int, maxNrClass)

		var i int
		for i = 0; i < l; i++ {
			thisLabel := int(prob.y[i])
			var j int
			for j = 0; j < nrClass; j++ {
				if thisLabel == label[j] {
					count[j]++
					break
				}
			}
			if j == nrClass {
				if nrClass == maxNrClass {
					// TODO make class to realloc
					maxNrClass *= 2
					newLabel := make([]int, maxNrClass)
					copy(newLabel, label)
					label = newLabel
					newCount := make([]int, maxNrClass)
					copy(newCount, count)
					count = newCount
				}
				label[nrClass] = thisLabel
				count[nrClass] = 1
				nrClass++
			}
		}
		for i = 0; i < nrClass; i++ {
			n1 := count[i]
			for j := i + 1; j < nrClass; j++ {
				n2 := count[j]
				if param.nu*float64(n1+n2)/2 > math.Min(float64(n1), float64(n2)) {
					return "specified nu is infeasible"
				}
			}
		}
	}
	return ""
}

func svmCheckProbabilityModel(model *svmModel) int {
	if ((model.param.svmType == C_SVC || model.param.svmType == NU_SVC) &&
		model.probA != nil && model.probB != nil) ||
		((model.param.svmType == EPSILON_SVR || model.param.svmType == NU_SVR) &&
			model.probA != nil) {
		return 1
	}
	return 0
}
