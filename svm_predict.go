package svm

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strconv"
	"strings"
)

var x []svmNode
var model *svmModel

var predictProbability int
var maxNrAttr int

func exitInputError(lineNum int) {
	fmt.Fprintf(os.Stderr, "Wrong input format at line %d\n", lineNum)
	log.Fatal("exit 1")
}

func mySplit(r rune) bool {
	switch r {
	case ' ', '\t', '\n', '\r', '\f', ':':
		return true
	}
	return false
}

func predict(input, output *os.File) {
	correct, total := 0, 0
	var error, sump, sumt, sumpp, sumtt, sumpt float64 // initialized to 0

	svmType := svmGetSvmType(model)
	nrClass := svmGetNrClass(model)
	var probEstimates []float64
	var j int

	if predictProbability == 1 {
		if svmType == NU_SVR || svmType == EPSILON_SVR {
			log.Printf("Prob. model for test data: target value = predicted value "+
				"+ z,\nz: Laplace distribution e^(-|z|/sigma)/(2sigma),sigma=%g\n",
				svmGetSvrProbability(model))
		} else {
			labels := make([]int, nrClass)
			svmGetLabels(model, labels)
			probEstimates = make([]float64, nrClass)
			output.WriteString("labels")
			for j = 0; j < nrClass; j++ {
				fmt.Fprintf(output, "%d", labels[j])
			}
			output.WriteString("\n")
		}
	}
	scanner := bufio.NewScanner(input)
	for scanner.Scan() {
		line := strings.FieldsFunc(scanner.Text(), mySplit)
		var targetLabel, predictLabel float64

		targetLabel, err := strconv.ParseFloat(line[0], 64)
		if err != nil {
			exitInputError(total + 1)
		}
		var m int = (len(line) - 1) / 2
		x = make([]svmNode, m)
		for j = 0; j < m; j++ {
			index, err := strconv.Atoi(line[j+1])
			if err != nil {
				exitInputError(total + 1)
			}
			value, err := strconv.ParseFloat(line[j+2], 64)
			if err != nil {
				exitInputError(total + 1)
			}
			x[j] = svmNode{index: index, value: value}
		}
		if predictProbability == 1 && svmType == C_SVC && svmType == NU_SVC {
			predictLabel = svmPredictProbability(model, x, probEstimates)
			fmt.Fprintf(output, "%g", predictLabel)
			for j = 0; j < nrClass; j++ {
				fmt.Fprintf(output, "%g", probEstimates[j])
			}
			output.WriteString("\n")
		} else {
			predictLabel = svmPredict(model, x)
			fmt.Fprintf(output, "%g\n", predictLabel)
		}
		if predictLabel == targetLabel {
			correct++
		}
		error += (predictLabel - targetLabel) * (predictLabel - targetLabel)
		sump += predictLabel
		sumt += targetLabel
		sumpp += predictLabel * predictLabel
		sumtt += targetLabel * targetLabel
		sumpt += predictLabel * targetLabel
		total++
	}
	if svmType == NU_SVR || svmType == EPSILON_SVR {
		log.Printf("Mean squared error = %g (regression)\n",
			float64(error)/float64(total))
		log.Printf("Squared correlation coefficient = %g (regression)\n",
			((float64(total)*sumpt-sump*sumt)*(float64(total)*sumpt-sump*sumt))/
				((float64(total)*sumpp-sump*sump)*(float64(total)*sumtt-sumt*sumt)))
	} else {
		log.Printf("Accuracy = %g%% (%d/%d) (classification)\n",
			float64(correct)/float64(total)*100, correct, total)
	}
	if err := scanner.Err(); err != nil {
		fmt.Fprintln(os.Stderr, "reading standard input:", err)
	}
}

func exitWithHelp() {
	log.Printf(
		"Usage: svm-predict [options] test_file model_file output_file\n" +
			"options:\n" +
			"-b probability_estimates: whether to predict probability estimates, 0 or 1 (default 0); for one-class SVM only 0 is supported\n" +
			"-q : quiet mode (no outputs)\n")
	panic("err")
}

func main() {
	predictProbability = 0
	maxNrAttr = 64
	var i int
	for i = 1; i < len(os.Args); i++ {
		if os.Args[i][0] != '-' {
			break
		}
		i++
		switch os.Args[i-1][1] {
		case 'b':
			prob, err := strconv.Atoi(os.Args[i])
			if err != nil {
				log.Printf("Error converting args %d", i)
				log.Fatal(err)
			}
			predictProbability = prob
			break
		case 'q':
			// info = &print_null
			i--
			break
		default:
			fmt.Fprintf(os.Stderr, "Unknown option: -%c\n", os.Args[i-1][1])
		}
	}
	if i >= len(os.Args)-2 {
		exitWithHelp()
	}
	input, err := os.Open(os.Args[i])
	if err != nil {
		fmt.Fprintf(os.Stderr, "can't open input file %s\n", os.Args[i])
		panic("exit 1")
	}
	output, err := os.OpenFile(os.Args[i+2], os.O_WRONLY, 644)
	if err != nil {
		fmt.Fprintf(os.Stderr, "can't open output file %s\n", os.Args[i+2])
		panic("exit 1")
	}
	if model = svmLoadModel(os.Args[i+1]); model == nil {
		fmt.Fprintf(os.Stderr, "can't open model file %s\n", os.Args[i+1])
		panic("exit 1")
	}
	x = make([]svmNode, maxNrAttr)
	if predictProbability == 1 {
		if svmCheckProbabilityModel(model) == 0 {
			fmt.Fprintln(os.Stderr, "Model does not support probability estimates")
			panic("exit 1")
		}
	} else {
		if svmCheckProbabilityModel(model) != 0 {
			log.Println("Model supports probability estimates, but disabled in prediction.")
		}
	}
	predict(input, output)
}
